# fixrs-tests

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

# Questions

0. Fork this project
0. Make the FIXRS logo 200px by 200px
0. List all fixrs in a `<p>` tag
0. Add a 150px gravatar next to their name using the vue-gravatar component https://github.com/JiriChara/vue-gravatar
0. Add a blue 10px border to any fixrs having the name 'Thomas' or 'Arthur'
0. Add a mention `(on vacation)` next to the fixrs' name if he is on vacation
0. When clicking on the logo, it should fire an event from within the Logo component. When the event is fired, the App.vue :: congrats() function should be called.

